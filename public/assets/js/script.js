//Comments 

//Comments are sections in the code that is not read by the language.
//It is for the user's to know what the code does

//In Java Script, there are two types of comments:
/*
   
    1. The single line comment, denoted by //
    2. The multi line comment, denoted by /* */

    alert("Hello World");

    /*
    Syntax and Statements

    Syntax contains the rules needed to create a statement in a programming language
    statements are instruction we tell the application to perform.
    
    Statements end with a semicolon(;) as best practice
    /*

    variable and constants

    variables and contants are containers for data that can be used to store information and retrieve or manipulate in the future.

    variables can contain values that can change as the program runs.

    To create a variable, we use the "let" keyword (Keywords are special words in a programming language)

    To create a constant, we use the "const" keyword.

    */
     

    let productName = "Web Developer";
    let productPrice = "182923"
    const PI = 892.32;



    console.log(productName);
    console.log(productPrice);
    console.log(PI);


    console.log("Hello World")
    console.log(12345);
    console.log("I am selling a " + productName);

    //String
    let fullName ="Brandon B. Brandon";
    let schoolName = "Zuitt Coding Bootcamp";
    let userName = "brandon00";
    
    //Number
    let age = 28;
    let numberOfPets= 5;
    let desiredGrade= 98.5;

    //Boolean
    let isSingle = true;
    let hasEaten = false;

    //Undefined
    let petName; //undefined because we didn't assign any value

    //Null
    let grandChildName = null; //you didn't put a value on purpose.

    //Object
    let person = {
    	firstName: "Jobert",
    	lastName : "Boyd",
    	age : 15,
    	petName : "Whitey"
    };

    //Arithmetic operator has 5 operations
    let num1 = 28;
    let num2 = 75;
    
    let sum = num1 + num2;
    console.log(sum);
    let difference = num1 - num2;
    console.log(difference);
    let product = num1 * num2;
    console.log(product);
    let qoutient = num2/ num1;
    console.log(qoutient);
    let remainder = num2 % num1;

    console.log(remainder);

    //You can actually combine the assignment and arithmetic operators


    let num4=5;
    ++num4;
    console.log(num4);

    let numA = 65;
    let numB = 65;
    console.log(numA == numB);

     let statement1 = "true";
     let statement2 = true;

    console.log(statement2 == statement1);
    console.log(statement2 === statement1);
    


    let numC = 25;
    let numD = 45;

    console.log(numC > numD);  //flase
    console.log(numC < numD);  //true
    console.log(numC >= numD); //false


    let isTall = false;
    let isDark = true;
    let isHandsome = true;
    let didPassStandard = isTall && isDark;
    console.log(didPassStandard); //false
     

    let didPassLowerStandard = isTall || isDark;
    console.log(didPassLowerStandard);

    let result = !isTall || (isDark && isHandsome); //true -> ! reverses the value
    //Note you can combine comparison, relational and logical operators

    /*

    Funtions - are groups of statement that perform a single action to prevent code duplication.
    A function has two main concepts
    1. Function declaration (definition)
    2.Fuction invocation. (calling)

    */

    function createFullName(fName, mName, lName){
    	return fName + mName + lName;
    }


    /*
    function is a keyword in JS that says that it is a function 
    createFullName is the name of the fucntion, it should descriptive of what it does fName, mName, lName are called parameters, these are the inputs that the function needs in order to work. Note: parameters are optional, meaning you can create a function without any parameters.
    return is a keyword in JS that returns the resulting value after the function is completely run. Note: this is also optional if you don't want any returning values.
       */


    //Function invocation
    let fullName1 = createFullName("Brandon", "Ray", "Smith");
    console.log(fullName1); //BrandonRaySmith;
    let fullName2 = createFullName("John", "Robert", "Smith");
    console.log(fullName2); //JohnRobertSmith
    let fN = "Jobert";
    let mN = "Bob";
    let lN = "Garcia";
    let fullName3 = createFullName(fN, mN, lN);
    console.log(fullName3);
    let fullName4 = createFullName(mN, fN, lN);
    console.log(fullName4);


    
    function addTwo(x,y){
         return (x + y) * 2;
    }

    
    console.log(addTwo(5,10));










